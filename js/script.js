// Активация кнопки меню
const iconMenu = document.querySelector('.menu__icon');
if (iconMenu){
    const menuNav = document.querySelector('.menu__nav');
    iconMenu.addEventListener("click", function (e) {
        document.body.classList.toggle('_lock'); // Блокирует прокрутку контента сайта, когда ты в меню
        iconMenu.classList.toggle('_active'); // Анимация кнопки меню
        menuNav.classList.toggle('_active'); // выезжает меню
    });
}

// Добавил slick slider для Section Customer and Section Our Team
$(document).ready(function(){
    // Листаем аватары
    $('.avatar-slider').slick({
        infinite: true,
        arrows:false,
        dots:true,
        centerMode: true,
        variableWidth: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 1,
        asNavFor: '.descrption-slider',
        responsive: [
            {
              breakpoint: 576,
              settings: {
                dots:false,
              }
            }
        ]
    });
    // Связан с аватаром, запрещаем листать
    $('.descrption-slider').slick({
        arrows:false,
        dots:false,
        initialSlide: 1,
        fade: true,
        draggable: false,
        swipe:false,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.avatar-slider',
    });

    // Вертикальный слайдер для отзывов
    $('.customers__slider').slick({
        infinite: true,
        arrows:true,
        dots:false,
        slidesToShow: 2,
        slidesToScroll: 1,
        vertical: true,
        verticalSwiping: true,
        swipe:false,
        responsive: [
            {
              breakpoint: 576,
              settings: {
                arrows:false,
                dots:true,
                slidesToShow: 1,
                vertical: false,
                verticalSwiping: false,
                swipe:true,
              }
            },
        ]
    });
});

// Схлопывам информационные блоки в Section Services
let services = document.getElementsByClassName('services__header');
for(let i = 0; i < services.length; i++) {
  services[i].addEventListener('click', function () {
    this.classList.toggle('_active');
    let content = this.nextElementSibling;
    if (content.style.maxHeight && content.style.paddingTop) {
      content.style.maxHeight = null;
      content.style.paddingTop = null;
    } else {
      content.style.maxHeight = 500 + 'px';
      content.style.paddingTop = 25 + 'px';
    }
  })
}
